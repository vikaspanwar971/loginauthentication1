const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const authRoute = require('./routes/auth');

dotenv.config();

//Connect to DB
//Enter MongoDB URL
url = process.env.DB_CONNECT;
console.log(url);

mongoose.connect(url, { useNewUrlParser: true }, () => {
  console.log('connected to Database');
});

app.use(express.json());

app.use('/api/user', authRoute);

app.listen(3000, () => console.log('Server is running'));
