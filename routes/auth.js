const router = require('express').Router();
const User = require('../model/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {
  registerAuthentication,
  loginAuthentication,
} = require('../validation');

//Register
router.post('/register', async (req, res) => {
  const { error } = registerAuthentication(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const emailExists = await User.findOne({ email: req.body.email });
  if (emailExists) return res.status(400).send('Email already exists');

  //password encryption
  const salt = await bcrypt.genSalt(10);
  const encrptPassword = await bcrypt.hash(req.body.password, salt);

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: encrptPassword,
  });
  try {
    const saveUser = await user.save();
    res.send({ user: user._id });
    console.log(saveUser);
  } catch (err) {
    res.status(400).send(err);
  }
});

//Log in
router.post('/login', async (req, res) => {
  const { error } = loginAuthentication(req.body);

  if (error) return res.status(400).send(error.details[0].message);

  const userExits = await User.findOne({ email: req.body.email });
  if (!userExits) return res.status(400).send('Email does not exits.');

  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send('Password is not correct');

  //Create a TOKEN and assign it

  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_KEY);
  res.header('auth-token', token).send(token);
});

module.exports = router;
